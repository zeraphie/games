import BasicElement from "./dependencies/BasicElement";
import Snowball from "./games/Snowball/_snowballConfig";
import SantaDash from './games/SantaDash/_santaDashConfig';
import Helper from "./dependencies/Helper";

export default class HomeScreen extends BasicElement {
    constructor(engine, loader){
        super(['game-screen', 'home-screen'], engine.wrapper);

        this.engine = engine;
        this.loader = loader;
        this.games = [];
        this.introMessage = this.buildIntroMessage();
        this.tileWrapper = this.buildTileWrapper();

        this.activeGame = null;

        this.addGames([Snowball, SantaDash]);

        this.appendToWrapper();
    }

    /**
     * Add the games to the homeScreen
     *
     * @param games
     */
    addGames(games){
        // Loop through all the games and list them on the front
        for(let i = 0, len = games.length; i < len; i++){
            let game = games[i];

            game.tile = new BasicElement([game.name, 'tile'], this.tileWrapper.element);
            game.tile.appendContent(game.name);
            game.tile.appendToWrapper();

            game.tile.element.addEventListener('pointerup', () => {
                this.hide();

                this.loader.show()
                    .child.element.setAttribute('data-progress', '0');

                this.addActiveGame(game);
            });

            this.games.push(game);
        }

        return this.games;
    }

    /**
     * Set the clicked game as the active game
     *
     * @param game
     */
    addActiveGame(game){
        this.activeGame = game.name;

        this.engine.addClass(this.activeGame);

        game.startGame(this.engine, this.loader);

        return game;
    }

    /**
     * Remove the currently active game
     *
     * @returns {null|*}
     */
    removeActiveGame(){
        let temp = this.activeGame;

        this.engine.removeClass(Helper.classify(this.activeGame));

        this.games.find((game) => game.name === temp).removeGame();

        this.activeGame = null;

        return temp;
    }

    /**
     * Build the intro message element
     *
     * @returns {BasicElement}
     */
    buildIntroMessage(){
        let introMessage = new BasicElement('intro-message', this.element);
        introMessage.appendContent('Pick a game below');
        introMessage.appendToWrapper();

        return introMessage;
    }

    /**
     * Build the tile wrapper element
     *
     * @returns {BasicElement}
     */
    buildTileWrapper(){
        let tileWrapper = new BasicElement('tile-wrapper', this.element);
        tileWrapper.appendToWrapper();

        return tileWrapper;
    }
}
