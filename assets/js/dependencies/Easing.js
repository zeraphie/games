/*==============================================================================
 ES6 implementation of https://gist.github.com/gre/1650294
==============================================================================*/
export default class Easing {
    /**
     * Linear rate of change
     *
     * @param t
     * @returns {*}
     */
    static linear(t){
        return t;
    }

    /**
     * Accelerate the rate of change
     *
     * @param t
     * @returns {number}
     */
    static easeInQuad(t){
        return t * t;
    }

    /**
     * Decelerate the rate of change
     *
     * @param t
     * @returns {number}
     */
    static easeOutQuad(t){
        return t * (2 - t);
    }

    /**
     * Accelerate rate of change until halfway, then decelerate
     *
     * @param t
     * @returns {number}
     */
    static easeInOutQuad(t){
        return (t < 0.5) ? (2 * t * t) : (-1 + (4 - 2 * t) * t);
    }

    /**
     * Accelerate the rate of change
     *
     * @param t
     * @returns {number}
     */
    static easeInCubic(t){
        return t * t * t;
    }

    /**
     * Decelerate the rate of change
     *
     * @param t
     * @returns {number}
     */
    static easeOutCubic(t){
        return (--t) * t * t + 1;
    }

    /**
     * Accelerate rate of change until halfway, then decelerate
     *
     * @param t
     * @returns {number}
     */
    static easeInOutCubic(t){
        return (t < 0.5) ? (4 * t * t * t) : ((t - 1) * (2 * t - 2) * (2 * t - 2) + 1);
    }
}
