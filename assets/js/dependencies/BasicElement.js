import Helper from "./Helper";

export default class BasicElement {
    constructor(classes, wrapper = 'body'){
        this.element = this.constructor.createElement(classes);
        this.stored = null;

        // Set the wrapper of the element to be an html element
        this.wrapper = wrapper;

        if(!(this.wrapper instanceof HTMLElement)){
            this.wrapper = document.querySelector(wrapper);
        }
    }

    /**
     * Create the element and store the reference for it
     *
     * @param classes
     * @returns {Element}
     */
    static createElement(classes){
        let element = document.createElement('div');

        if(typeof classes === 'string'){
            element.className = Helper.classify(classes);
        }

        if(classes instanceof Array){
            element.classList.add(...classes.map(classie => Helper.classify(classie)));
        }

        return element;
    }

    /**
     * Remove the current element from the dom
     */
    removeElement(){
        this.element.parentNode.removeChild(this.element);
    }

    /**
     * Add classes to the element
     *
     * @param classes
     */
    addClass(...classes){
        this.element.classList.add(...classes.map(classie => Helper.classify(classie)));
    }

    /**
     * Remove all the classes on this element that match a regex expression
     *
     * @param regex
     * @returns {BasicElement}
     */
    removeClass(regex){
        // Convert classList into an array then filter based on the regex passed
        let filtered = [].slice.call(this.element.classList).filter(c => c.match(regex));

        // Remove all the filtered classes
        this.element.classList.remove(...filtered);

        return this;
    }

    /**
     * Append this element to the wrapper given
     *
     * @returns {BasicElement}
     */
    appendToWrapper(){
        this.wrapper.appendChild(this.element);

        return this;
    }

    /**
     * Prepend this element to the wrapper given
     *
     * @returns {BasicElement}
     */
    prependToWrapper(){
        this.wrapper.insertBefore(this.element, this.wrapper.firstChild);

        return this;
    }

    /**
     * Append the html to this element
     *
     * @param html
     * @returns {BasicElement}
     */
    appendContent(html){
        this.element.innerHTML = html;

        return this;
    }

    /**
     * Hide the element
     *
     * @returns {BasicElement}
     */
    hide(){
        if(this.element){
            this.stored = this.element;
            this.removeElement();
            this.element = null;
        }

        return this;
    }

    /**
     * Show the element
     *
     * @returns {BasicElement}
     */
    show(){
        if(this.element === null){
            this.element = this.stored;
            this.appendToWrapper();
        }

        return this;
    }
}
