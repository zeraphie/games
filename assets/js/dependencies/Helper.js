export default class Helper {
    /**
     * Bind callback to the resize and orientation change events
     *
     * @param callback
     */
    static onresize(callback){
        if(typeof callback === 'function'){
            ['resize', 'orientationchange'].forEach((event) => {
                window.addEventListener(event, callback);
            });
        }
    }

    /**
     * Get an object's key by its value, only works on flat objects
     *
     * @param obj
     * @param value
     * @returns {*}
     * @constructor
     */
    static ObjectKeyByValue(obj, value){
        return Object.keys(obj).find(key => obj[key] === value);
    }

    /**
     * Convert a string to be class friendly
     *
     * @param token
     * @returns {string}
     */
    static classify(token){
        return token.replace(' ', '-').toLowerCase();
    }

    /**
     * Add some text to the log for mobile testing
     *
     * @param text
     */
    static log(text){
        let log = document.querySelector('.log');

        if(log !== null){
            log.innerText = text;
        }
    }

    /**
     * Generate an unbiased number between a range
     *
     * @param min
     * @param max
     * @returns {*}
     */
    static randomNumberInRange(min, max){
        return Math.round(Math.random() * (max - min)) + min;
    }

    /**
     * Load all promises with a progress function
     *
     * @param promises
     * @param progressCallback
     * @returns {Promise.<*[]>}
     */
    static loadWithProgress(promises, progressCallback){
        if(typeof progressCallback === 'function'){
            let i = 0, len = promises.length;

            progressCallback(0);

            promises.forEach(promise => {
                Promise.resolve(promise).then(() => {
                    i++;
                    progressCallback(i * 100 / len);
                });
            });
        }

        return Promise.all(promises);
    }
}

