import Easing from "../../dependencies/Easing";
import FlashMessage from "../../engine/elements/FlashMessage";

export default {
    /**
     * Reset the player's position for this game
     *
     * @returns {*}
     */
    playerPosition(){
        this.player_original = JSON.parse(JSON.stringify(this.engine.player.original));

        let wrapper_rect = this.engine.player.wrapper.getBoundingClientRect();

        this.player_original.position.x = this.engine.player.rect.width;
        this.player_original.position.y = wrapper_rect.height - this.engine.player.rect.height * 2;

        this.engine.player.move(
            this.player_original.position,
            this.player_original.angle
        );

        return this.player_original;
    },

    /**
     * Test if the player was hit by an obstacle
     *
     * @param obstacle
     * @returns {*}
     */
    playerHit(obstacle){
        if(obstacle.type === 'circle'){
            return obstacle.collidedCircles(this.engine.player);
        }

        if(obstacle.type === 'rect'){
            return obstacle.collidedRectToCircle(this.engine.player);
        }

        return false;
    },

    /**
     * Lose a life
     *
     * @param obstacle
     * @returns {Snowball}
     */
    loseLife(obstacle){
        this.destroyObstacle(obstacle);

        this.lives--;

        this.scoreboard.amendScores({lives: this.scores.lives});

        new FlashMessage(`Hah. You Died!`);

        return this;
    },

    /**
     * Make the player jump
     *
     * @returns {*}
     */
    jumpHook(){
        // If the jump action hasn't been made, do nothing!
        if(!this.actions.jump.active){
            return false;
        }

        // The jump height in pixels
        let jumpHeight = 80;
        // Half the duration of the jump
        let animationTime = 300;

        // Setup a promise for the going up that executes until the animationTime
        // is complete
        let up = new Promise((resolve, reject) => {
            // Get the current time passed since the jump started decimal
            let time = (new Date() - this.actions.jump.timestamp) / animationTime;

            // If the time passed is greater than the animation time, clamp it
            // and resolve the promise
            if(time > 1){
                time = 1;

                resolve();
            }

            // Use the time taken to add a new delta with an eased tween
            let height = Easing.easeOutQuad(time) * jumpHeight;

            // Move the player to the new height
            this.engine.player.move(
                {
                    x: this.player_original.position.x,
                    y: this.player_original.position.y - height,
                }
            );

            this.tracer.trace(this.engine.player, ...this.obstacles);
        });

        // When the jump is at it's highest point, start making the player fall
        return up.then(
            () => new Promise((resolve, reject) => {
                // Calculate the time taken since the jump started - the time
                // of the going up
                let time = ((new Date() - this.actions.jump.timestamp) / animationTime) - 1;

                // Clamp and resolve if the time taken is more than the animation time
                if(time > 1){
                    time = 1;

                    resolve();
                }

                // Calculate the new delta for the player to be at (i.e. falling)
                let height = jumpHeight - (Easing.easeInCubic(time) * jumpHeight);

                // move the player
                this.engine.player.move(
                    {
                        x: this.player_original.position.x,
                        y: this.player_original.position.y - height,
                    }
                );

                this.tracer.trace(this.engine.player, ...this.obstacles);
            }).then(
                () => {
                    // After the animation has finished, reactivate the ability
                    // to jump and reset the timestamp
                    this.actions.jump.timestamp = null;
                    this.actions.jump.active = false;
                }
            )
        );
    }
}
