import SantaDash from "./index";

export default {
    name: 'Santa Dash',
    intro: "Get santa to deliver his presents!",
    game: null,
    engine: null,

    /**
     * The assets used for this game
     *
     * @returns {[null,null,null]}
     */
    get assets(){
        return [
            {
                url: this.engine.url + 'build/audio/test.mp3',
                type: '.mp3',
                name: 'Test mp3'
            },
            {
                url: this.engine.url + 'build/audio/chgame.wav',
                type: '.wav',
                name: 'Background Music'
            }
        ];
    },

    /**
     * Start a new instance of the game
     *
     * @param engine
     * @param loader
     * @returns {SantaDash}
     */
    startGame(engine, loader){
        this.engine = engine;

        let game = new SantaDash(this.engine);

        game.loadAssets(
            this.assets,
            progress => loader.child.element.setAttribute(
                'data-progress',
                Math.round(progress).toString()
            )
        ).then(() => {
            setTimeout(() => {
                loader.hide();

                game.intro(this.intro);
            }, 310);
        });

        return this.game = game;
    },

    /**
     * Remove references for the game
     */
    removeGame(){
        this.game.reset();

        this.engine = null;
        this.game = null;
    }
};
