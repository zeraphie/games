import Helper from "../../dependencies/Helper";
import Enemy from "../../engine/elements/Enemy";

export default {
    moveObstacles(){
        // Move all the obstacles to the left and detect if they hit the player
        this.obstacles.forEach((obstacle) => {
            // If the obstacle has gone off the left side of the screen, count
            // it as dodged
            if(obstacle.current.position.x + obstacle.rect.width <= 0){
                this.dodgedObstacle(obstacle);

                return;
            }

            // Decrease number of lives if player is hit by an obstacle
            // and respawn the obstacle on the right side of the screen
            if(this.playerHit(obstacle)){
                this.loseLife(obstacle);

                return;
            }

            let displacement = obstacle.calculateDisplacement(
                this.engine.frames,
                {
                    x: 1 + (0.2 * (this.level - 1)),
                    y: 0
                }
            );

            // Move the obstacles to the left in an accelerated way
            obstacle.move({
                x: obstacle.current.position.x - displacement.x,
                y: obstacle.current.position.y - displacement.y
            });
        });
    },

    /**
     * Successfully dodged an obstacle
     *
     * @param obstacle
     * @returns {SantaDash}
     */
    dodgedObstacle(obstacle){
        this.dodged++;

        this.scoreboard.amendScores({dodged: this.scores.dodged});

        this.destroyObstacle(obstacle);

        return this;
    },

    /**
     * Calculate a new position for the obstacle
     *
     * @param obstacle
     * @returns {*}
     */
    obstaclePosition(obstacle){
        let obstacleType = Helper.randomNumberInRange(1, 3);

        let baseline = this.engine.rect.height - this.engine.player.rect.height * 2;

        let modifier = obstacle.max_speed * (0.2 * (obstacleType - 1));

        obstacle.speed = obstacle.max_speed - modifier;

        // Bottom rectangular shape
        if(obstacleType === 1){
            obstacle.setType('rect');

            obstacle.move({
                x: this.engine.rect.width,
                y: baseline
            });
        }

        // Middle circle
        if(obstacleType === 2){
            obstacle.setType('circle');

            obstacle.move({
                x: this.engine.rect.width,
                y: baseline - 25
            });
        }

        // Top circle
        if(obstacleType === 3){
            obstacle.setType('circle');

            obstacle.move({
                x: this.engine.rect.width,
                y: baseline - 70
            });
        }

        return obstacle;
    },

    /**
     * Spawn an obstacle at a random position off the right hand side of the screen
     *
     * @returns {Number}
     */
    addObstacle(){
        let obstacle = new Enemy('circle', 'obstacle', this.engine.element);

        this.obstaclePosition(obstacle);

        return this.obstacles.push(obstacle);
    },

    /**
     * Add a number of obstacles to the pool for interaction
     *
     * @param number
     * @returns {Array}
     */
    addObstacles(number){
        for(let i = 0; i < number; i++){
            this.addObstacle();
        }

        return this.obstacles;
    },

    /**
     * Remove the obstacle from the engine
     *
     * @param searchFor
     */
    destroyObstacle(searchFor){
        let index = searchFor;

        if(searchFor instanceof Enemy){
            index = this.obstacles.findIndex(obstacle => obstacle === searchFor);
        }

        if(index > -1){
            this.obstacles[index].remove();
            this.obstacles.splice(index, 1);

            return true;
        }

        return false;
    },

    /**
     * Destroy all obstacles
     *
     * @returns {Array}
     */
    destroyObstacles(){
        if(!this.obstacles){
            return [];
        }

        let len = this.obstacles.length;

        if(len <= 0){
            return [];
        }

        while(len--){
            this.destroyObstacle(len);
        }

        return this.obstacles;
    }
}
