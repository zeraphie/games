import Scene from "../../engine/core/Scene";
import Tracer from "../../engine/core/Tracer";
import Keyboard from "../../engine/controllers/keyboard/Keyboard";
import Player from "../../engine/elements/Player";
import {baseWithTraits} from "../../dependencies/baseWithTraits";
import SantaDashPlayer from "./SantaDashPlayer";
import SantaDashObstacles from "./SantaDashObstacles";
import Scores from "../../engine/elements/Scores";
import FlashMessage from "../../engine/elements/FlashMessage";

export default class SantaDash extends baseWithTraits(Scene, SantaDashPlayer, SantaDashObstacles) {
    constructor(engine, difficulty = 0){
        super(engine);

        // Set game styles
        this.engine.setSize(200, 768)
            .setBackground('#0c8')
            .centerGame();

        // Setup the controller for this game
        let controller = new Keyboard().attachEvents();
        this.engine.connectController(controller);
        this.engine.controller.onkeyup = this.onkeyup.bind(this);

        // Attach the player for this game
        let player = new Player('player', this.engine.element, false);
        this.engine.connectPlayer(player);
        this.playerPosition();

        this.tracer = new Tracer(this);

        // Setup the variables for this game
        this.difficulty = difficulty;
        this.spawnBoundary = 250;

        this.setDefaults();
    }

    /**
     * Dynamically get the formatting for scores
     *
     * @returns {{lives: string, dodged: string, level: string}}
     */
    get scores(){
        return {
            lives: `Lives: ${this.lives}`,
            dodged: `Dodged: ${this.dodged}`,
            level: `Level: ${this.level}`
        };
    }

    /**
     * Number of lives the player has
     *
     * @returns {number}
     */
    static get max_lives(){
        return 1;
    }

    /**
     * Bind the keyboard actions
     *
     * @param key
     */
    onkeyup(key){
        if(key === 'JUMP'){
            if(!this.actions.jump.active){
                this.actions.jump.timestamp = new Date();
                this.actions.jump.active = true;
            }
        }
    }

    /**
     * Setup the defaults for the santa dash
     *
     * @returns {SantaDash}
     */
    setDefaults(){
        // Scoreboard
        if(this.scoreboard){
            this.scoreboard.hide();
        }
        this.scoreboard = null;

        this.obstacles = this.destroyObstacles();
        this.dodged = 0;
        this.level = 1;
        this.lives = this.constructor.max_lives;
        this.spawnFlag = false;

        this.actions = {
            jump: {
                active: false,
                timestamp: null
            }
        };

        this.tracer.hide();

        return this;
    }

    /**
     * Setup the game
     *
     * @returns {SantaDash}
     */
    setup(){
        this.setDefaults();

        this.scoreboard = new Scores(this.scores, this.engine.wrapper);

        super.setup();

        return this;
    }

    /**
     * Play the game!
     *
     * @returns {SantaDash}
     */
    play(){
        this.engine.play(() => {
            // End the engine if the lives have all been lost
            if(this.lives <= 0){
                this.end();

                return;
            }

            // Spawn a new obstacle if there are no obstacles that have an x
            // position greater than spawn boundary - Make this ok for mobile?
            if(!this.obstacles.some(obstacle => obstacle.current.position.x > this.spawnBoundary - (10 * (this.level - 1)))){
                this.addObstacle();
            }

            this.moveObstacles();

            // Level up every 15 seconds
            if(this.level < Math.ceil(this.time_taken / 15)){
                this.levelUp();
            }

            this.tracer.trace(this.engine.player, ...this.obstacles);

            this.jumpHook();
        });

        return this;
    }

    end(additional){
        let otherMessages = {};

        let lives_message = `It took you ${this.time_taken} seconds to run away from ${this.dodged} enemies`;
        let level_message = `But hey, at least you got to level ${this.level}`;

        if(this.level === 1){
            level_message = `...seriously? You died to the first level? I've seen grannies play better than you`;
        }

        if(this.level === 2){
            level_message = `At least you didn't die on the first level, I guess that's an accomplishment`;
        }

        if(this.level >= 3 && this.level <= 5){
            level_message = `I'll concede that you're not a complete moron, you lasted until level ${this.level}`;
        }

        if(this.level > 5){
            level_message = `Congratulations! You have progressed to human levels of gaming! You reached level ${this.level}`;
        }

        if(additional){
            otherMessages.additional = additional;
        }

        this.setDefaults();

        super.end(
            Object.assign(
                {
                    lives_message,
                    level_message
                },
                otherMessages
            )
        );

        // Reset the player position to the center of the screen
        this.playerPosition();
    }

    /**
     * Reset the snowball instance (i.e. remove controllers/players/etc)
     *
     * @returns {Snowball}
     */
    reset(){
        this.tracer.hide();
        this.engine.removeClass(/level-/);

        this.engine.controller.remove();
        this.engine.controller = null;

        this.engine.player.remove();
        this.engine.player = null;

        this.engine.resetGameRect();

        return this;
    }

    /**
     * Remove existing level classes from the engine and add the current level
     *
     * @returns {Snowball}
     */
    addLevelClass(){
        this.engine.removeClass(/level-/);

        this.engine.element.classList.add('level-' + this.level);

        return this;
    }

    /**
     * Apply level up logic
     *
     * @returns {SantaDash}
     */
    levelUp(){
        this.level++;

        this.addLevelClass();

        if(this.scoreboard !== null){
            this.scoreboard.amendScores({level: this.scores.level});
        }

        new FlashMessage(`Die Faster!`);

        if(this.bgm){
            this.bgm.source.playbackRate.value = 1 + (0.1 * (this.level - 1));
        }

        return this;
    }
}
