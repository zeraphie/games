import {baseWithTraits} from "../../dependencies/baseWithTraits";

import Scene from "../../engine/core/Scene";
import Tracer from "../../engine/core/Tracer";
import Joystick from "../../engine/controllers/joystick/index";

import Player from "../../engine/elements/Player";
import Scores from "../../engine/elements/Scores";

import FlashMessage from "../../engine/elements/FlashMessage";

// Snowball traits, these are actually normal objects and are required
import SnowballEnemies from "./SnowballEnemies";
import SnowballPowerUps from "./SnowballPowerUps";

export default class Snowball extends baseWithTraits(Scene, SnowballEnemies, SnowballPowerUps){
    constructor(engine, difficulty = 0){
        super(engine);

        // Attach the controller for this engine
        let controller = new Joystick(
            'joystick-base',
            'joystick-shaft',
            this.engine.wrapper
        ).attachEvents();
        this.engine.connectController(controller);

        // Attach the player for this engine
        let player = new Player('player', this.engine.element);
        this.engine.connectPlayer(player);

        this.tracer = new Tracer(this);

        this.difficulty = difficulty;

        this.setDefaults();
    }

    /**
     * Dynamically get the formatting for scores
     *
     * @returns {{lives: string, dodged: string, level: string}}
     */
    get scores(){
        return {
            lives: `Lives: ${this.lives}`,
            dodged: `Dodged: ${this.dodged}`,
            level: `Level: ${this.level}`
        };
    }

    /**
     * Number of lives the player has
     *
     * @returns {number}
     */
    static get max_lives(){
        return 3;
    }

    /**
     * Sets the engine variables to default settings
     *
     * @returns {Snowball}
     */
    setDefaults(){
        this.removePowerUps();

        // Game variables
        this.enemies = this.destroyEnemies();
        this.level = 1;
        this.lives = this.constructor.max_lives;
        this.dodged = 0;

        // Scoreboard
        if(this.scoreboard){
            this.scoreboard.hide();
        }
        this.scoreboard = null;

        this.tracer.hide();

        this.addLevelClass();

        return this;
    }

    /**
     * Setup the side scroller
     *
     * @returns {Snowball}
     */
    setup(){
        this.setDefaults();

        this.addEnemies(25 + (this.difficulty * 10));

        this.scoreboard = new Scores(this.scores, this.engine.wrapper);

        super.setup();

        return this;
    }

    /**
     * Play the engine!
     *
     * Remove enemies on colliding with them or if they escape, adding to the
     * lives or escape values
     *
     * @returns {Snowball}
     */
    play(){
        this.engine.play(() => {
            // End the engine if the lives have all been lost
            if(this.lives <= 0){
                this.end();

                return;
            }

            this.movePowerUps();
            this.moveEnemies();
            this.generatePowerUps();

            // Level up every 15 seconds
            if(this.level < Math.ceil(this.time_taken / 15)){
                this.levelUp();
            }

            this.tracer.trace(this.engine.player, ...this.enemies, ...this.power_ups);
        });

        return this;
    }

    /**
     * Finish the engine and show the end screen
     *
     * @param additional
     */
    end(additional){
        let otherMessages = {};

        let lives_message = `It took you ${this.time_taken} seconds to run away from ${this.dodged} enemies`;
        let level_message = `But hey, at least you got to level ${this.level}`;

        if(this.level === 1){
            level_message = `...seriously? You died to the first level? I've seen grannies play better than you`;
        }

        if(this.level === 2){
            level_message = `At least you didn't die on the first level, I guess that's an accomplishment`;
        }

        if(this.level >= 3 && this.level <= 5){
            level_message = `I'll concede that you're not a complete moron, you lasted until level ${this.level}`;
        }

        if(this.level > 5){
            level_message = `Congratulations! You have progressed to human levels of gaming! You reached level ${this.level}`;
        }

        if(additional){
            otherMessages.additional = additional;
        }

        this.setDefaults();

        super.end(
            Object.assign(
                {
                    lives_message,
                    level_message
                },
                otherMessages
            )
        );
    }

    /**
     * Reset the snowball instance (i.e. remove controllers/players/etc)
     *
     * @returns {Snowball}
     */
    reset(){
        this.tracer.hide();
        this.engine.removeClass(/level-/);
        this.removePowerUps();

        this.engine.controller.remove();
        this.engine.controller = null;

        this.engine.player.remove();
        this.engine.player = null;

        return this;
    }

    /**
     * Remove existing level classes from the engine and add the current level
     *
     * @returns {Snowball}
     */
    addLevelClass(){
        this.engine.removeClass(/level-/);

        this.engine.element.classList.add('level-' + this.level);

        return this;
    }

    /**
     * Apply level up logic
     *
     * @returns {Snowball}
     */
    levelUp(){
        this.level++;

        this.addLevelClass();

        if(this.scoreboard !== null){
            this.scoreboard.amendScores({level: this.scores.level});
        }

        new FlashMessage(`Die Faster!`);

        if(this.bgm){
            this.bgm.source.playbackRate.value = 1 + (0.1 * (this.level - 1));
        }

        return this;
    }

    /**
     * Lose a life
     *
     * @param enemy
     * @returns {Snowball}
     */
    loseLife(enemy){
        enemy.randomPosition(this.enemyPosition(enemy));

        // Ignore if currently invincible
        if(this.concurrent.invincibility){
            new FlashMessage(`I'll kill you soon!`);

            return this;
        }

        // Remove shield if currently shielded
        if(this.concurrent.shielded){
            new FlashMessage(`Hah! Got your shield`);

            this.engine.player.element.classList.remove('shielded');

            this.concurrent.shielded = false;

            return this;
        }

        this.lives--;

        this.scoreboard.amendScores({lives: this.scores.lives});

        new FlashMessage(`Hah. You Died!`);

        return this;
    }

    /**
     * Test if the player was hit by an enemy
     *
     * @param enemy
     * @returns {*}
     */
    playerHit(enemy){
        if(enemy.type === 'circle'){
            return enemy.collidedCircles(this.engine.player);
        }

        if(enemy.type === 'rect'){
            return enemy.collidedRectToCircle(this.engine.player);
        }

        return false;
    }
}
