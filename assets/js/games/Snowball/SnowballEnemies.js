import Helper from "../../dependencies/Helper";

import Enemy from "../../engine/elements/Enemy";

/**
 * Snowball enemies trait, this manages all the enemy functionality in the
 * Snowball class
 */
export default {
    moveEnemies(){
        // Move all the enemies to the left and detect if they hit the player
        this.enemies.forEach((enemy) => {
            // If the enemy has gone off the left side of the screen, count
            // it as dodged
            if(enemy.current.position.x + enemy.rect.width <= 0){
                this.dodgedEnemy(enemy);

                return;
            }

            // Decrease number of lives if player is hit by an enemy
            // and respawn the enemy on the right side of the screen
            if(this.playerHit(enemy)){
                this.loseLife(enemy);

                return;
            }

            let displacement = enemy.calculateDisplacement(
                this.engine.frames,
                {
                    x: 1 + (0.2 * (this.level - 1)),
                    y: 0
                }
            );

            // Move the enemies to the left in an accelerated way
            enemy.move({
                x: enemy.current.position.x - displacement.x,
                y: enemy.current.position.y - displacement.y
            });
        });
    },

    /**
     * Successfully dodged an enemy
     *
     * @param enemy
     * @returns {Snowball}
     */
    dodgedEnemy(enemy){
        this.dodged++;

        this.scoreboard.amendScores({dodged: this.scores.dodged});

        enemy.randomPosition(this.enemyPosition(enemy));

        return this;
    },

    /**
     * Calculate a new position for the enemy
     *
     * @returns {{x: {min: *, max: number}, y: {min: number, max: number}}}
     */
    enemyPosition(enemy){
        return {
            x: {
                min: this.engine.rect.width + 0,
                max: this.engine.rect.width + this.engine.rect.width - enemy.rect.width
            },
            y: {
                min: 0,
                max: this.engine.rect.height - enemy.rect.height
            }
        };
    },

    /**
     * Spawn an enemy at a random position off the right hand side of the screen
     *
     * @returns {Number}
     */
    addEnemy(type = 'circle'){
        let enemy = new Enemy(type, 'enemy', this.engine.element);

        // Set a random speed for the enemy up to a max speed
        enemy.speed = Helper.randomNumberInRange(
            enemy.max_speed * 0.5,
            enemy.max_speed
        );

        // Set a random position off to the right hand of the screen
        enemy.randomPosition(this.enemyPosition(enemy));

        return this.enemies.push(enemy);
    },

    /**
     * Add a number of enemies to the pool for interaction
     *
     * @param number
     * @returns {Array}
     */
    addEnemies(number){
        // IE doesn't like the dynamic way......................................
        for(let i = 0; i < number; i++){
            // Add circle or rectangle enemies for fun - Proof of concept
            // for different collision detection
            this.addEnemy((i % 2 === 0) ? 'circle' : 'rect');
        }

        return this.enemies;
    },

    /**
     * Remove the enemy from the engine
     *
     * @param index
     */
    destroyEnemy(index){
        if(index < 0){
            return;
        }

        this.enemies[index].remove();
        this.enemies.splice(index, 1);
    },

    /**
     * Destroy all enemies
     *
     * @returns {Array}
     */
    destroyEnemies(){
        if(!this.enemies){
            return [];
        }

        let len = this.enemies.length;

        if(len <= 0){
            return [];
        }

        while(len--){
            this.destroyEnemy(len);
        }

        return this.enemies;
    }
}
