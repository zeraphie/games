# Snowball
In this game the goal is to dodge all the enemies, and perhaps collect the power ups. **The game *will* abuse you.**

## Level Ups
Level ups currently happen every 75 enemies dodges

## Enemies
This game contains recognition for both circle and rectangular collision so the enemy sprites reflect this

## Power Ups
Currently the following are power ups and are separated into three sections

### Positive power ups
These power ups contain only beneficial effects
- Life Up - This power up gains a life if lives are not maxed out
- Shield - This power up absorbs a hit that would lose a life
- Invincibility - Gives the player 3 seconds of invincibility

### Neutral power ups
These power ups contain effects that do not directly lose lives
- Vertical Flip - This power up flips the entire game by 180 degrees
- Invert Colours - This power up inverts all the colours in the game
    - *Note: this uses the filter css property which breaks on I.E.*

### Detrimental power ups
These power ups contain effects that directly cause the loss of life or ending of the game
- Instant Death - Instantly gives a game over
