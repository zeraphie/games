import Snowball from "./index";

export default {
    name: 'Snowball',
    intro: "Surely you can dodge everything? It's pretty easy",
    game: null,
    engine: null,

    /**
     * The assets used for this game
     *
     * @returns {[null,null,null]}
     */
    get assets(){
        return [
            {
                url: this.engine.url + 'build/audio/test.mp3',
                type: '.mp3',
                name: 'Test mp3'
            },
            {
                url: this.engine.url + 'build/audio/chgame.wav',
                type: '.wav',
                name: 'Alan test wav'
            },
            {
                url: this.engine.url + 'build/audio/2nd.wav',
                type: '.wav',
                name: 'Background Music'
            }
        ];
    },

    /**
     * Start a new instance of the game
     *
     * @param engine
     * @param loader
     * @returns {Snowball}
     */
    startGame(engine, loader){
        this.engine = engine;

        let game = new Snowball(this.engine);

        game.loadAssets(
            this.assets,
            progress => loader.child.element.setAttribute(
                'data-progress',
                Math.round(progress).toString()
            )
        ).then(() => {
            setTimeout(() => {
                loader.hide();

                game.intro(this.intro);
            }, 310);
        });

        return this.game = game;
    },

    /**
     * Remove references for the game
     */
    removeGame(){
        this.game.reset();

        this.engine = null;
        this.game = null;
    }
};
