import Helper from "../../dependencies/Helper";

import PowerUp from "../../engine/elements/PowerUp";
import FlashMessage from "../../engine/elements/FlashMessage";

/**
 * Snowball power up trait, this manages all the power up functionality in
 * the Snowball class
 */
export default {
    movePowerUps(){
        this.power_ups.forEach((powerUp, index) => {
            // If the enemy has gone off the left side of the screen, count
            // it as dodged
            if(powerUp.current.position.x + powerUp.rect.width <= 0){
                this.destroyPowerUp(index);

                return;
            }

            // Decrease number of lives if player is hit by an enemy
            // and respawn the enemy on the right side of the screen
            if(this.playerHit(powerUp)){
                this.powerUp(powerUp, index);

                return;
            }

            let displacement = powerUp.calculateDisplacement(
                this.engine.frames,
                {
                    x: 1 + (0.2 * (this.level - 1)),
                    y: 0
                }
            );

            // Move the power ups to the left in an accelerated way
            powerUp.move({
                x: powerUp.current.position.x - displacement.x,
                y: powerUp.current.position.y - displacement.y
            });
        });
    },

    generatePowerUps(){
        /*--------------------------------------
         Positive power ups
        --------------------------------------*/
        // Add a life up power up on every 25 dodges
        if(this.counters.lifeUps < Math.ceil(this.dodged / 25)){
            this.addPowerUp('life-up');
            this.counters.lifeUps++;
        }

        // Add a shield power up on every 25 dodges
        if(this.counters.shield < Math.ceil(this.dodged / 25)){
            this.addPowerUp('shield');
            this.counters.shield++;
        }

        // Add an invincibility power up on every 100 dodges
        if(this.counters.invincibility < Math.ceil(this.dodged / 100)){
            this.addPowerUp('invincibility');
            this.counters.invincibility++;
        }

        /*--------------------------------------
         Neutral power ups
        --------------------------------------*/
        // Add a vertical reverse power up on every 50 dodges
        if(this.counters.verticalReverse < Math.ceil(this.dodged / 50)){
            this.addPowerUp('vertical-reverse');
            this.counters.verticalReverse++;
        }

        // Add an invert power up on every 75 dodges
        if(this.counters.invertColours < Math.ceil(this.dodged / 75)){
            this.addPowerUp('invert-colours');
            this.counters.invertColours++;
        }

        /*--------------------------------------
         Detrimental power ups
        --------------------------------------*/
        // Add an instant death power up on every 100 dodges
        if(this.counters.instantDeath < Math.ceil(this.dodged / 100)){
            this.addPowerUp('instant-death');
            this.counters.instantDeath++;
        }
    },

    /**
     * Power up the player with stacking buffs
     *
     * @param powerUp
     * @param index
     */
    powerUp(powerUp, index){
        /*--------------------------------------
         Positive power ups
        --------------------------------------*/
        // On gaining the life-up power up, gain a life and update the engine
        if(powerUp.power_up === 'life-up'){
            if(this.lives < this.constructor.max_lives){
                this.lives++;

                this.scoreboard.amendScores({lives: this.scores.lives});

                new FlashMessage(`Aw, you gained a life`);
            }
        }

        // Add a shield that absorbs one hit
        if(powerUp.power_up === 'shield'){
            this.concurrent.shielded = true;

            this.engine.player.element.classList.add('shielded');
        }

        // Add invincibility for x amount of time
        if(powerUp.power_up === 'invincibility'){
            new FlashMessage(`Dammit, you can't die!`);

            this.concurrent.invincibility = true;

            this.engine.player.element.classList.add('invincible');

            setTimeout(() => {
                this.concurrent.invincibility = false;

                this.engine.player.element.classList.remove('invincible');
            }, 3000);
        }

        /*--------------------------------------
         Neutral power ups
        --------------------------------------*/
        // Toggle the engine 180deg vertically
        if(powerUp.power_up === 'vertical-reverse'){
            this.engine.paused = true;

            setTimeout(() => {
                if(this.engine.element.style.transform === '') {
                    this.engine.element.style.transform = 'rotate(180deg)';
                } else {
                    this.engine.element.style.transform = '';
                }

                setTimeout(() => {
                    this.engine.paused = false;
                    this.engine.effects.flipped = !this.engine.effects.flipped;
                }, 500);
            }, 100);
        }

        // Invert the colours of the screen
        if(powerUp.power_up === 'invert-colours'){
            if(this.engine.wrapper.style.filter === ''){
                this.engine.wrapper.style.filter = 'invert(100%)';
            } else {
                this.engine.wrapper.style.filter = '';
            }
        }

        /*--------------------------------------
         Detrimental power ups
        --------------------------------------*/
        // On gaining the instant-death power up, do a engine over
        if(powerUp.power_up === 'instant-death' && !this.concurrent.invincibility){
            this.end(`Instant death? ...really? You do know that you're not meant to collect red shapes... right?`);

            return;
        }

        this.destroyPowerUp(index);
    },

    /**
     * Remove all the effects that are created with this.powerUp
     *
     * @returns {Snowball}
     */
    removePowerUps(){
        // Concurrent power up config
        this.concurrent = {
            invincibility: false,
            shielded: false,
        };

        // Reset the counters for the powerups
        this.counters = {
            lifeUps: 1,
            shield: 1,
            invincibility: 1,
            verticalReverse: 1,
            invertColours: 1,
            instantDeath: 1,
        };

        // Remove effects that are attached to the engine itself
        for(let effect in this.engine.effects){
            if(this.engine.effects.hasOwnProperty(effect)){
                this.engine.effects[effect] = false;
            }
        }

        // Reset engine effects (like the vertical flip)
        this.engine.element.removeAttribute('style');
        this.engine.wrapper.removeAttribute('style');

        this.power_ups = this.destroyPowerUps();

        return this;
    },

    /**
     * Spawn a powerup at a random position off the right hand side of the screen
     *
     * @returns {Number}
     */
    addPowerUp(type = 'life-up'){
        let powerUp = new PowerUp(type, this.engine.element);

        // Set a random speed for the powerUp up to a max speed
        powerUp.speed = Helper.randomNumberInRange(
            powerUp.max_speed * 0.5,
            powerUp.max_speed
        );

        // Set a random position off to the right hand of the screen
        powerUp.randomPosition(this.powerUpPosition(powerUp));

        return this.power_ups.push(powerUp);
    },

    /**
     * Remove the power up from the engine
     *
     * @param index
     */
    destroyPowerUp(index){
        if(index < 0){
            return;
        }

        this.power_ups[index].remove();
        this.power_ups.splice(index, 1);
    },

    /**
     * Destroy all power ups
     *
     * @returns {Array}
     */
    destroyPowerUps(){
        if(!this.power_ups){
            return [];
        }

        let len = this.power_ups.length;

        if(len <= 0){
            return [];
        }

        while(len--){
            this.destroyPowerUp(len);
        }

        return this.power_ups;
    },

    /**
     * Calculate a new position for the power up
     *
     * @returns {{x: {min: *, max: number}, y: {min: number, max: number}}}
     */
    powerUpPosition(powerUp){
        return {
            x: {
                min: this.engine.rect.width + 0,
                max: this.engine.rect.width + this.engine.rect.width - powerUp.rect.width
            },
            y: {
                min: 0,
                max: this.engine.rect.height - powerUp.rect.height
            }
        };
    }
}
