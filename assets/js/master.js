// Polyfills
import "pepjs";

// These polyfills are required for it to work in IE
import "babel-polyfill";
import "whatwg-fetch";
import "classlist-polyfill";

// Game elements
import Engine from "./engine/core/Engine";
import HomeScreen from "./HomeScreen";
import Loader from "./engine/elements/Loader";

window.onload = () => {
    // Setup the engine instance
    const engine = new Engine('game', '.wrapper');
    const loader = new Loader('loader').hide();

    // Setup the home screen
    window.homeScreen = new HomeScreen(engine, loader);
};
