import GameElement from './GameElement';
import AssetLoader from './AssetLoader';

export default class Engine extends GameElement {
    constructor(selector, parent){
        super(selector, parent);

        this.url = this.url || window.location.href;

        this.raf = null;
        this.frames = 0;
        this.playing = false;
        this.start_time = null;
        this.paused_frames = 0;

        this.fps = 60;

        this.assets = new AssetLoader();

        this.effects = {
            flipped: false
        };
    }

    /**
     * Get how long the game has been paused for in seconds
     *
     * @returns {number}
     */
    get paused_time(){
        return this.paused_frames / this.fps;
    }

    /**
     * Set the size of the game window
     *
     * @param height
     * @param width
     * @returns {Engine}
     */
    setSize(height, width){
        if(typeof height === 'number'){
            this.element.style.height = height;
        }

        if(typeof width === 'number'){
            this.element.style.width = width;
        }

        this.calculateRect();

        return this;
    }

    /**
     * Center the game on the screen
     *
     * @returns {Engine}
     */
    centerGame() {
        this.element.style.top = '50%';
        this.element.style.left = '50%';
        this.element.style.transform = `translate3d(-50%, -50%, 0)`;

        this.calculateRect();

        return this;
    }

    /**
     * Reset game rect
     *
     * @returns {Engine}
     */
    resetGameRect(){
        this.element.removeAttribute('style');

        this.calculateRect();

        return this;
    }

    /**
     * Set the background of the game window
     *
     * @param value
     * @returns {Engine}
     */
    setBackground(value){
        this.element.style.background = value;

        return this;
    }

    /**
     * Connect the player to the engine
     *
     * @param player
     * @returns {Engine}
     */
    connectPlayer(player){
        this.player = player;

        return this;
    }

    /**
     * Connect the controller to the engine
     *
     * @param controller
     * @returns {Engine}
     */
    connectController(controller){
        this.controller = controller;

        return this;
    }

    /**
     * Control the player using the controller
     *
     * @returns {Engine}
     */
    movePlayerWithController(){
        if(this.controller.state !== 'active'){
            return this;
        }

        // Calculate the displacement value using the magnitude of the
        // joystick and the amount of frames that have passed
        let displacement = this.player.calculateDisplacement(
            this.frames,
            {
                x: this.controller.shaft.current.vector.x / this.controller.boundary,
                y: this.controller.shaft.current.vector.y / this.controller.boundary
            }
        );

        // Add the displacement to the current position of the player
        let newpos = this.player.clamp(
            {
                x: (this.effects.flipped) ? this.player.current.position.x - displacement.x : this.player.current.position.x + displacement.x,
                y: (this.effects.flipped) ? this.player.current.position.y - displacement.y : this.player.current.position.y + displacement.y,
            },
            {
                width: this.rect.width,
                height: this.rect.height
            }
        );

        // Move the player relative to the original position and rotate it
        // in accordance with the joystick
        this.player.move(
            {
                x: newpos.x,
                y: newpos.y,
            },
            (this.effects.flipped) ? this.controller.shaft.current.angle - 180 : this.controller.shaft.current.angle
        );
    }

    /**
     * The engine loop that is called after every frame finishes rendering
     *
     * @param callback
     */
    loop(callback){
        if(this.controller.type === 'joystick'){
            this.movePlayerWithController();
        }

        if(typeof callback === 'function'){
            callback(this);
        }

        this.frames++;
    }

    /**
     * This is the engine loop
     *
     * @param callback
     */
    play(callback){
        this.playing = true;

        if(this.start_time === null){
            this.start_time = new Date();
        }

        this.render(callback, this.fps);
    }

    /**
     * End the engine by clearing the interval and frames
     *
     * @param callback
     */
    end(callback){
        this.playing = false;

        window.cancelAnimationFrame(this.raf);
        this.raf = null;

        if(typeof callback === 'function'){
            callback(this);
        }

        this.start_time = null;
        this.paused_frames = 0;
        this.frames = 0;
    }

    /**
     * Render the scene frame by frame using requestAnimationFrame
     *
     * @param afterRender
     * @param fps
     */
    render(afterRender = () => {}, fps){
        let fpsInterval = 1000 / fps;

        const animate = () => {
            // For some reason, this needs to be before the loop
            this.raf = window.requestAnimationFrame(animate);

            let now = new Date();
            let elapsed = now - this.start_time;

            if (elapsed > fpsInterval) {
                this.start_time = now - (elapsed % fpsInterval);

                if(this.playing){
                    this.loop(afterRender);
                } else {
                    this.paused_frames++;
                }
            }
        };

        animate();
    }
}
