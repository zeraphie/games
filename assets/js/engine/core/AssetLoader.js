// https://www.npmjs.com/package/audiofile
import AudioFile from 'audiofile';

import Helper from "../../dependencies/Helper";

export default class AssetLoader {
    constructor(){
        this.loaded = [];

        this.webAudioSupported = (
            window.AudioContext ||
            window.webkitAudioContext ||
            window.mozAudioContext ||
            window.oAudioContext ||
            window.msAudioContext
        );
    }

    /**
     * Load all the files into an array
     *
     * @returns {Promise.<*[]>}
     */
    loadAll(files, progressCallback){
        return Helper.loadWithProgress(
            files.map(
                file => {
                    let found = -1;

                    // Search the currently loaded assets for the current file
                    // and return it if it's found
                    found = this.loaded.findIndex(
                        loaded => JSON.stringify(loaded.info) === JSON.stringify(file)
                    );

                    if(found > -1){
                        return this.loaded[found];
                    }

                    // Search if the current file has been loaded and rename
                    // if found
                    found = this.loaded.findIndex(
                        loaded => loaded.info.url === file.url
                    );

                    if(found > -1){
                        this.loaded[found].info.name = file.name;

                        return this.loaded[found];
                    }

                    // Audio File - with a whitelist of files
                    if(['.mp3', '.wav'].indexOf(file.type) > -1){
                        if(!this.webAudioSupported){
                            console.log('Web Audio API not supported. Get a better browser.');

                            return false;
                        }

                        let temp = new AudioFile({
                            url: file.url,
                        });

                        return temp.load().then(data => {
                            let loaded = {
                                info: file,
                                type: 'audio',
                                audio: temp
                            };

                            this.loaded.push(loaded);

                            return loaded;
                        });
                    }

                    return false;
                }
            ), progressCallback
        );
    }
}
