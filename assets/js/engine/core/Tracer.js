import Helper from "../../dependencies/Helper";

export default class Tracer {
    constructor(gameType){
        this.game_type = gameType;
        this.stored = this.createTracer();
        this.element = null;
        this.ctx = null;
        this.wrapper = document.querySelector('.game');

        this.colour = '#0c8';
        this.strokeWidth = 2;
    }

    /**
     * Trace all the objects passed into it if the global variables are set
     *
     * @param objects
     */
    trace(...objects){
        if(
            window.TRACE
                ||
            window.localStorage.getItem('TRACE') === 'true'
                ||
            window.sessionStorage.getItem('TRACE') === 'true'
        ){
            this.show();
            this.traceElements(...objects);
        } else {
            this.hide();
        }
    }

    /**
     * Hide the element
     *
     * @returns {Tracer}
     */
    hide(){
        this.clearTrace();

        if(this.element !== null){
            this.stored = this.element;
            this.removeElement();
            this.ctx = null;
            this.element = null;
        }

        this.game_type.engine.removeClass('tracing');

        return this;
    }

    /**
     * Show the element
     *
     * @returns {Tracer}
     */
    show(){
        if(this.element === null){
            this.element = this.stored;
            this.appendToWrapper();
            this.ctx = this.element.getContext('2d');
            this.stored = null;
        }

        this.game_type.engine.addClass('tracing');

        return this;
    }

    /**
     * Add the tracer to the wrapper
     *
     * @returns {Tracer}
     */
    appendToWrapper(){
        this.wrapper.appendChild(this.element);

        return this;
    }

    /**
     * Remove the current element from the dom
     */
    removeElement(){
        this.element.parentNode.removeChild(this.element);
    }

    /**
     * Create the Tracer element
     *
     * @returns {Element}
     */
    createTracer(){
        let element = document.createElement('canvas');

        element.classList.add('tracer');

        element.setAttribute('width', this.game_type.engine.rect.width);
        element.setAttribute('height', this.game_type.engine.rect.height);

        Helper.onresize(() => {
            element.setAttribute('width', this.game_type.engine.rect.width);
            element.setAttribute('height', this.game_type.engine.rect.height);
        });

        return element;
    }

    /**
     * Trace the elements passed through to the canvas
     *
     * @param elements
     * @returns {boolean}
     */
    traceElements(...elements){
        if(this.ctx === null){
            return false;
        }

        let rect = this.element.getBoundingClientRect();

        this.ctx.clearRect(0, 0, rect.width, rect.height);

        elements.forEach((element) => {
            // element.current.position should be the center point of the
            // element for the collision detection to work as intended

            if(element.type === 'rect'){
                this.drawRect({
                    x: element.current.position.x - (this.strokeWidth * 2),
                    y: element.current.position.y - (this.strokeWidth * 2),
                    width: element.rect.width - this.strokeWidth,
                    height: element.rect.height - this.strokeWidth
                });
            }

            if(element.type === 'circle'){
                let radius = (element.rect.width - this.strokeWidth) / 2;

                this.drawCircle({
                    x: element.current.position.x + radius - this.strokeWidth * 2,
                    y: element.current.position.y + radius - this.strokeWidth * 2,
                    r: radius
                });
            }
        });

        return true;
    }

    /**
     * Clear the tracer
     *
     * @returns {boolean}
     */
    clearTrace(){
        if(this.ctx === null){
            return false;
        }

        let rect = this.element.getBoundingClientRect();

        this.ctx.clearRect(0, 0, rect.width, rect.height);

        return true;
    }

    /**
     * Draw a rect on the canvas
     *
     * @param rect
     * @returns {boolean}
     */
    drawRect(rect){
        if(this.ctx === null){
            return false;
        }

        this.ctx.beginPath();

        this.ctx.rect(
            rect.x,
            rect.y,
            rect.width,
            rect.height
        );

        this.ctx.lineWidth = this.strokeWidth;
        this.ctx.strokeStyle = this.colour;

        this.ctx.stroke();

        return true;
    }

    /**
     * Draw a circle on the canvas
     *
     * @param circle
     * @returns {boolean}
     */
    drawCircle(circle){
        if(this.ctx === null){
            return false;
        }

        this.ctx.beginPath();

        this.ctx.arc(
            circle.x,
            circle.y,
            circle.r,
            0,
            2 * Math.PI
        );

        this.ctx.lineWidth = this.strokeWidth;
        this.ctx.strokeStyle = this.colour;

        this.ctx.stroke();

        return true;
    }
}
