import Helper from '../../dependencies/Helper';
import BasicElement from "../../dependencies/BasicElement";

export default class GameElement extends BasicElement {
    constructor(classes, parent = 'body'){
        super(classes, parent);

        this.appendToWrapper();
    }

    /**
     * Append this element to the wrapper given
     *
     * @returns {BasicElement}
     */
    appendToWrapper(){
        super.appendToWrapper();

        this.calculateRect();

        // Recalculate the rect on resizing
        Helper.onresize(() => {
            this.calculateRect();
        });

        return this;
    }

    /**
     * Extend the bounding client rect with a center point and radius
     *
     * @returns {*}
     */
    calculateRect(){
        let rect = this.element.getBoundingClientRect();

        return this.rect = Object.assign(
            rect,
            {
                center: {
                    x: rect.left + rect.width / 2,
                    y: rect.top + rect.height / 2
                }
            }
        );
    }
}
