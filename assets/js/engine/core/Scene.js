import GameScreen from "../elements/GameScreen";
import Mute from "../elements/Mute";

export default class Scene {
    constructor(engine){
        this.engine = engine;

        this.level = 1;
        this.started_at = new Date();

        this.pausableElement = false;

        this.assets = [];
        this.bgm = null;
        this.mute = null;

        this.restrictPause = this.restrictPause.bind(this);
        this.stopStart = this.stopStart.bind(this);

        this.pauseScreen = new GameScreen(
            {
                paused: 'Paused',
                instructions: 'If you ever feel like dying some more, press space or tap/click on the screen to unpause'
            },
            this.engine.wrapper
        );

        this.pauseScreen.hide();
    }

    /**
     * Get the current time elapsed in the game, minus the paused time
     *
     * @returns {number}
     */
    get time_taken(){
        return Math.round(
            ((new Date() - this.started_at) / 1000) - this.engine.paused_time
        );
    }

    /**
     * Load the game with it's assets using promises
     *
     * @param assets
     * @param progressCallback
     * @returns {Promise.<*[]>}
     */
    loadAssets(assets, progressCallback){
        // Do a clone of the assets because otherwise the loading changes the
        // original set..................
        this.assets = (assets instanceof Array) ? JSON.parse(JSON.stringify(assets)) : [];

        return this.engine.assets.loadAll(this.assets, progressCallback).then(() => {
            if(this.assets.some(asset => ['.mp3', '.wav'].indexOf(asset.type))){
                if(this.mute === null){
                    this.mute = new Mute(this);
                }
            } else {
                if(this.mute === null){
                    this.mute.remove();
                    this.mute = null;
                }
            }
        });
    }

    /**
     * Get all the audio files in the scene
     *
     * @returns {Array.<*>}
     */
    get audio_files(){
        return this.engine.assets.loaded.filter(asset => asset.type === 'audio');
    }

    /**
     * Get an audio attached to this scene by name
     *
     * @param name
     * @returns {*}
     */
    getAudio(name){
        if(!this.audio_files.length){
            // No audio files found
            return false;
        }

        let found = this.assets.find(asset => asset.name === name);

        if(!found){
            // Audio file with the given name not found
            return false;
        }

        // Audio has been found so play it!
        return this.audio_files.find(
            // Compare the two objects with stringify for exact match
            // At this point, there will be a find if assets have been loaded!
            file => JSON.stringify(file.info) === JSON.stringify(found)
        ).audio;
    }

    /**
     * Get all the audio files for the scene with a callback on the individual
     * audio element
     *
     * @param callback
     * @returns {*}
     */
    getAllAudio(callback){
        let audio_files = this.engine.assets.loaded.filter(asset => asset.type === 'audio');

        if(!audio_files.length){
            // No audio files found
            return false;
        }

        audio_files.forEach((audio_file) => {
            if(typeof callback === 'function'){
                callback(audio_file.audio);
            }
        });

        return audio_files;
    }

    /**
     * Stop all audio in the scene
     *
     * @returns {boolean}
     */
    stopAllAudio(){
        this.getAllAudio(audio => audio.stop());

        if(this.mute){
            this.mute.remove();
            this.mute = null;
        }

        return true;
    }

    /**
     * Add the pause and play functionality to the scene
     *
     * @param e
     */
    stopStart(e){
        const pausable = () => {
            if(this.engine.playing){
                this.engine.playing = false;

                this.pauseScreen.show();
            } else {
                this.pauseScreen.hide();

                this.engine.playing = true;
            }
        };

        if(e.which === 32 && this.engine.controller.type !== 'keyboard'){
            this.pausableElement = false;

            pausable();
        }

        if(e.target === this.engine.element && this.pausableElement){
            pausable();
        }

        if(this.pauseScreen.element instanceof HTMLElement && this.pausableElement){
            if(e.target === this.pauseScreen.element || this.pauseScreen.element.contains(e.target)){
                this.pauseScreen.hide();

                this.engine.playing = true;
            }
        }
    }

    /**
     * Restrict the pause to only be on pausable elements
     *
     * @param e
     */
    restrictPause(e){
        if(this.engine.controller.unpausable()){
            this.pausableElement = true;
        } else {
            this.pausableElement = false;
        }
    }

    /**
     * Do the intro for the game type
     *
     * @returns {GameScreen}
     */
    intro(introText){
        return new GameScreen(
            {
                text: introText,
                instructions: 'Use the joystick to control the player',
                pause_message: 'Did you know? If you need a break from being bad, press space or tap/click on the screen to pause',
                action: 'Play'
            },
            this.engine.wrapper,
            () => {
                this.bgm = this.getAudio('Background Music');

                if(this.bgm){
                    this.bgm.play().loop(true);
                    this.bgm.source.playbackRate.value = 1;

                    if(this.assets.mute && this.assets.mute.muted){
                        this.bgm.volume(0);
                    }
                }

                this.setup();
            }
        );
    }

    /**
     * Setup everything to start the game
     *
     * @returns {Scene}
     */
    setup(){
        this.started_at = new Date();

        document.addEventListener('pointerdown', this.restrictPause, false);

        ['keyup', 'pointerup'].forEach((event) => {
            document.addEventListener(event, this.stopStart, false);
        });

        if(this.bgm){
            this.bgm.play();
        }

        this.play();

        return this;
    }

    /**
     * Play the game! - Basic version
     *
     * @returns {Scene}
     */
    play(){
        this.engine.play();

        return this;
    }

    /**
     * Finish the game
     *
     * @returns {Scene}
     */
    end(messages){
        document.removeEventListener('pointerdown', this.restrictPause);

        ['keyup', 'pointerup'].forEach((event) => {
            document.removeEventListener(event, this.stopStart);
        });

        this.engine.end(() => {
            this.started_at = null;

            // Reset the player position to the center of the screen
            this.engine.player.move(
                this.engine.player.original.position,
                this.engine.player.original.angle
            );

            // Show the end message
            new GameScreen(
                Object.assign(messages, {action: 'Restart'}),
                this.engine.wrapper,
                () => {
                    this.setup();
                }
            ).addHomeScreen(this.stopAllAudio.bind(this));
        });

        return this;
    }
}
