import GameElement from "./GameElement";

export default class Interactable extends GameElement {
    constructor(classes, parent){
        super(classes, parent);
    }

    /**
     * Axis-Aligned Bounding Box collision detection - rect to rect
     *
     * @param thing
     * @returns {boolean}
     */
    collidedAABB(thing){
        return this.current.position.x                  < thing.current.position.x + (thing.rect.width / 2)     &&
            this.current.position.x + this.rect.width   > thing.current.position.x                              &&
            this.current.position.y                     < thing.current.position.y + (thing.rect.height / 2)    &&
            this.current.position.y + this.rect.height  > thing.current.position.y;
    }

    /**
     * Circle to Circle collision detection
     *
     * @param thing
     * @returns {boolean}
     */
    collidedCircles(thing){
        let circle1 = {
            x: this.current.position.x,
            y: this.current.position.y,
            r: this.rect.width / 2
        };

        let circle2 = {
            x: thing.current.position.x,
            y: thing.current.position.y,
            r: thing.rect.width / 2
        };

        let distance = Math.sqrt(
            Math.pow(circle1.x - circle2.x, 2) +
            Math.pow(circle1.y - circle2.y, 2)
        );

        return distance < circle1.r + circle2.r;
    }

    /**
     * Detect collision between this rectangle and the thing circle
     *
     * @param thing
     * @returns {*}
     */
    collidedRectToCircle(thing){
        let circle = {
            x: thing.current.position.x,
            y: thing.current.position.y,
            r: thing.rect.width / 2
        };

        let rect = {
            x: this.current.position.x - this.rect.width / 2,
            y: this.current.position.y - this.rect.height / 2,
            w: this.rect.width,
            h: this.rect.height
        };

        return this.constructor.rectCircleCollision(circle, rect);
    };

    /**
     * Detect collision between this circle and the thing rectangle
     *
     * @param thing
     * @returns {*}
     */
    collidedCircleToRect(thing){
        let circle = {
            x: this.current.position.x,
            y: this.current.position.y,
            r: this.rect.width / 2
        };

        let rect = {
            x: thing.current.position.x - thing.rect.width / 2,
            y: thing.current.position.y - thing.rect.height / 2,
            w: thing.rect.width,
            h: thing.rect.height
        };

        return this.constructor.rectCircleCollision(circle, rect);
    };

    /**
     * Detect if a rect and circle have collided
     *
     * @param circle
     * @param rect
     * @returns {boolean}
     */
    static rectCircleCollision(circle, rect){
        let distX = Math.abs(circle.x - rect.x);
        let distY = Math.abs(circle.y - rect.y);

        if(distX > (rect.w / 2 + circle.r)){
            return false;
        }
        if(distY > (rect.h / 2 + circle.r)){
            return false;
        }

        if(distX <= (rect.w / 2)){
            return true;
        }
        if(distY <= (rect.h / 2)){
            return true;
        }

        let dx = distX - rect.w / 2;
        let dy = distY - rect.h / 2;

        return (dx * dx + dy * dy <= (circle.r * circle.r));
    }
}
