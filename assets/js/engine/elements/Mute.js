import BasicElement from "../../dependencies/BasicElement";

export default class Mute extends BasicElement {
    constructor(game){
        super(['mute', 'material-icons'], game.engine.wrapper);

        this.game = game;

        this.toggleMute = this.toggleMute.bind(this);
        this.muted = false;

        this.appendToWrapper();
        this.attachEvents();
    }

    /**
     * Attach the events to the mute button
     *
     * @returns {Mute}
     */
    attachEvents(){
        this.element.addEventListener('pointerup', this.toggleMute, false);

        return this;
    }

    /**
     * Detach the events from the mute button
     *
     * @returns {Mute}
     */
    detachEvents(){
        this.element.removeEventListener('pointerup', this.toggleMute);

        return this;
    }

    /**
     * Toggle the mute
     */
    toggleMute(){
        this.element.classList.toggle('muted');

        this.muted = !this.muted;

        this.game.getAllAudio(audio => {
            if(this.muted){
                audio.volume(0);
            } else {
                audio.volume(1);
            }
        });
    }

    /**
     * Remove the mute button
     */
    remove(){
        this.detachEvents();

        this.element.remove();

        this.game.getAllAudio(audio => {
            audio.volume(1);
        });

        this.muted = false;
    }
}
