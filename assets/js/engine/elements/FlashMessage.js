import BasicElement from "../../dependencies/BasicElement";

export default class FlashMessage extends BasicElement {
    constructor(message, duration = 1000){
        super('flash-message', 'body');

        this.appendContent(message);
        this.appendToWrapper();

        this.element.style.transform = 'translate3d(-50%, -10px, 0)';
        this.element.style.opacity = 1;

        setTimeout(() => {
            this.element.style.transform = 'translate3d(-50%, 10px, 0)';
            this.element.style.opacity = 0;

            setTimeout(() => {
                this.removeElement();
            }, 310);
        }, duration);
    }
}
