import BasicElement from "../../dependencies/BasicElement";

export default class Loader extends BasicElement {
    constructor(type){
        super('loader-wrapper');

        this.type = type;

        this.child = new BasicElement('loader', this.element)
            .appendToWrapper();

        this.appendToWrapper();
    }
}
