import BasicElement from "../../dependencies/BasicElement";

export default class Scores extends BasicElement {
    constructor(scores, parent){
        super('scoreboard', parent);

        // Loop through all the messages and add them to the engine screen
        this.addScores(scores);

        this.prependToWrapper();
    }

    /**
     * Add all the scores to the scoreboard
     *
     * @param scores
     */
    addScores(scores){
        for(let score in scores){
            if(scores.hasOwnProperty(score)){
                this[score] = new BasicElement([score, 'score'], this.element);
                this[score].appendContent(scores[score]);
                this[score].appendToWrapper();
            }
        }
    }

    /**
     * Ammend all the scores on the scoreboard
     *
     * @param scores
     */
    amendScores(scores){
        for(let score in scores){
            if(scores.hasOwnProperty(score)){
                this[score].appendContent(scores[score]);
            }
        }
    }
}
