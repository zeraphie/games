import Enemy from "./Enemy";

export default class PowerUp extends Enemy {
    constructor(powerUp, parent){
        super('circle', [powerUp, 'material-icons', 'power-up'], parent);

        this.power_up = powerUp;
    }
}
