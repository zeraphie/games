import GameElement from '../core/GameElement';

export default class Player extends GameElement {
    constructor(selector, parent, center = true){
        super(selector, parent);
        this.current = this.original;

        this.threed = true;

        if(center){
            this.move(this.original.position, this.original.angle);
        }
    }

    /**
     * Get the type of the player (for collision and drawing purposes)
     *
     * @returns {string}
     */
    get type(){
        return 'circle';
    }

    /**
     * Maximum number of pixels that can be moved in 60 frames
     *
     * @returns {number}
     */
    get max_speed(){
        return 15;
    }

    /**
     * Number of frames taken to get to maximum speed
     *
     * 60fps x number of seconds
     *
     * @returns {number}
     */
    get frames_until_max_speed(){
        return 60 * 3;
    }

    /**
     * Original set of values for the player
     *
     * @returns {{position: {x: number, y: number}}}
     */
    get original(){
        let wrapper_rect = this.wrapper.getBoundingClientRect();

        return {
            position: {
                x: wrapper_rect.width / 2 - this.rect.width / 2,
                y: wrapper_rect.height / 2 - this.rect.height / 2
            },
            angle: 90
        };
    }

    /**
     * Restrict the player's movement to the window
     *
     * @param position
     * @param boundary
     * @returns {*}
     */
    clamp(position, boundary){
        if(position.x <= 0){
            position.x = 0;
        }

        if(position.x >= boundary.width - this.rect.width){
            position.x = boundary.width - this.rect.width;
        }

        if(position.y <= 0){
            position.y = 0;
        }

        if(position.y >= boundary.height - this.rect.height){
            position.y = boundary.height - this.rect.height;
        }

        return position;
    }

    /**
     * Move the player
     *
     * @param to
     * @param rotation
     * @param callback
     * @returns {*}
     */
    move(to, rotation = 90, callback){
        // For some reason Velocity refuses to work properly with this, so use
        // raw values to properly transform it...
        if(this.threed){
            this.element.style.transform = `translate3d(${to.x}px, ${to.y}px, 0px) rotate(${rotation}deg)`;
        } else {
            this.element.style.transform = `translateX(${to.x}px) translateY(${to.y}px) rotateZ(${rotation}deg)`;
        }

        this.current.position = to;
        this.current.angle = rotation;

        if(typeof callback === 'function'){
            callback();
        }

        return this.current.position;
    }

    /**
     * Calculate the difference in displacement to be added to the current
     * player position
     *
     * @param frames
     * @param weighting
     * @returns {{y: number, x: number}}
     */
    calculateDisplacement(frames, weighting){
        // Acceleration coefficient
        let acceleration = Math.pow(
            (frames <= this.frames_until_max_speed) ? frames / this.frames_until_max_speed : 1,
            2
        );

        return {
            y: this.max_speed * acceleration * weighting.y,
            x: this.max_speed * acceleration * weighting.x
        };
    }

    /**
     * Remove the player
     *
     * @returns {Player}
     */
    remove(){
        this.removeElement();

        return this;
    }
}
