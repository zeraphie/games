import BasicElement from "../../dependencies/BasicElement";
import HomeScreen from "../../HomeScreen";

export default class GameScreen extends BasicElement {
    constructor(messages, parent, callback = () => {}){
        super('game-screen', parent);

        // Loop through all the messages and add them to the engine screen
        for(let message in messages){
            if(messages.hasOwnProperty(message)){
                this[message] = new BasicElement(message, this.element);
                this[message].appendContent(messages[message]);
                this[message].appendToWrapper();
            }
        }

        // When the action button is clicked, remove the screen and do the
        // callback (which should be setup)
        if(this.action){
            this.action.element.addEventListener('pointerup', () => {
                this.hide();

                if(typeof callback === 'function'){
                    callback();
                }
            });
        }

        this.appendToWrapper();
    }

    /**
     * Add the back to home screen button
     *
     * @returns {HomeScreen}
     */
    addHomeScreen(callback){
        this.home = new BasicElement('home', this.element);
        this.home.appendContent('Back to home');
        this.home.appendToWrapper();

        this.home.element.addEventListener('pointerup', () => {
            this.hide();

            if(typeof callback === 'function'){
                callback();
            }

            window.homeScreen.removeActiveGame();

            window.homeScreen.show();
        });

        return window.homeScreen;
    }
}
