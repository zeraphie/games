import Interactable from "../core/Interactable";
import Helper from "../../dependencies/Helper";

export default class Enemy extends Interactable {
    constructor(type, classes, parent){
        if(classes instanceof Array){
            super([...classes, type], parent);
        } else {
            super([classes, type], parent);
        }

        this.current = this.original;
        this.speed = this.max_speed;

        this.wrapper_rect = this.wrapper.getBoundingClientRect();

        this.type = type;
    }

    /**
     * The maximum number of pixels that can be travelled in a frame
     *
     * @returns {number}
     */
    get max_speed(){
        return 10;
    }

    /**
     * Number of frames taken to get to maximum speed
     *
     * 60fps x number of seconds
     *
     * @returns {number}
     */
    get frames_until_max_speed(){
        return 60 * 30;
    }

    /**
     * Original set of values for the enemy
     *
     * @returns {{position: {x: number, y: number}}}
     */
    get original(){
        return {
            position: {
                x: this.rect.width / 2,
                y: this.rect.height / 2
            }
        };
    }

    /**
     * Set the type of the enemy
     *
     * @param type
     */
    setType(type){
        this.removeClass(this.type);

        this.type = type;

        this.addClass(this.type);
    }

    /**
     * Set the enemy to a random position within the boundaries
     *
     * @param boundary
     */
    randomPosition(boundary){
        let to = {
            x: Helper.randomNumberInRange(boundary.x.min, boundary.x.max),
            y: Helper.randomNumberInRange(boundary.y.min, boundary.y.max)
        };

        this.move(to);

        return this;
    }

    /**
     * Move the enemy to a location without animation
     *
     * @param to
     * @param callback
     * @returns {*}
     */
    move(to, callback){
        // For some reason Velocity refuses to work properly with this, so use
        // raw values to properly transform it...
        this.element.style.transform = `translate3d(${to.x}px, ${to.y}px, 0px)`;

        this.current.position = to;

        if(typeof callback === 'function'){
            callback();
        }

        return this.current.position;
    }

    /**
     * Calculate the difference in displacement to be added to the current
     * enemy position
     *
     * @param frames
     * @param weighting
     * @returns {{y: number, x: number}}
     */
    calculateDisplacement(frames, weighting){
        // Acceleration coefficient
        let acceleration = Math.pow(
            (frames <= this.frames_until_max_speed) ? frames / this.frames_until_max_speed : 1,
            1 / 3
        );

        return {
            x: this.speed * acceleration * weighting.x,
            y: this.speed * acceleration * weighting.y
        };
    }

    /**
     * Remove the enemy
     */
    remove(){
        this.wrapper.removeChild(this.element);
    }
}
