import JoystickElement from "./JoystickElement";
import JoystickShaft from "./JoystickShaft";

export default class Joystick {
    constructor(base, shaft, wrapper = 'body'){
        this.type = 'joystick';

        this.state = 'inactive';
        this.base = new JoystickElement(base, wrapper);
        this.shaft = new JoystickShaft(shaft, this.base.element);

        this.base.removeTouch();
        this.shaft.removeTouch();

        // Add the hooks
        this.onactivate = () => {};
        this.ondeactivate = () => {};
        this.ondrag = () => {};

        // Make sure the events are addable and removable!
        this.activate = this.activate.bind(this);
        this.deactivate = this.deactivate.bind(this);
        this.drag = this.drag.bind(this);
    }

    /**
     * The boundary for the joystick shaft to be limited by
     *
     * @returns {number}
     */
    get boundary(){
        return this.base.rect.radius * 0.75;
    }

    /**
     * Attach all the necessary events to the elements for the joystick to work
     *
     * @returns {Joystick}
     */
    attachEvents(){
        this.base.element.addEventListener('pointerdown', this.activate, false);
        document.addEventListener('pointerup', this.deactivate, false);
        document.addEventListener('pointermove', this.drag, false);

        return this;
    }

    /**
     * Remove all the events that were bound with attachEvents
     *
     * @returns {Joystick}
     */
    detachEvents(){
        this.base.element.removeEventListener('pointerdown', this.activate, false);
        document.removeEventListener('pointerup', this.deactivate, false);
        document.removeEventListener('pointermove', this.drag, false);

        // Make sure to reset everything!
        this.deactivate();

        return this;
    }

    /**
     * Activate the joystick (on clicking the base element)
     *
     * @returns {Joystick}
     */
    activate(){
        this.state = 'active';
        this.base.element.classList.add('active');

        if(typeof this.onactivate === 'function'){
            this.onactivate();
        }

        return this;
    }

    /**
     * Deactivate the joystick on mouseup/touchup as well as if the events are
     * unbound
     *
     * @returns {Joystick}
     */
    deactivate(){
        // Only fire this if the state was active in the first place!
        if(this.state !== 'active'){
            return this;
        }

        this.state = 'inactive';
        this.base.element.classList.remove('active');

        // Make sure to move it back to the center!
        this.shaft.move(
            this.shaft.original.vector,
            () => {
                // Reset all the relevant attributes
                this.shaft.element.removeAttribute('style');
                this.shaft.current = this.shaft.original;

                if(typeof this.ondeactivate === 'function'){
                    this.ondeactivate();
                }
            }
        );

        return this;
    }

    /**
     * Move the joystick shaft
     *
     * @param e
     * @returns {Joystick}
     */
    drag(e){
        // If it isn't active, don't do anything
        if(this.state !== 'active'){
            return this;
        }

        // Move the joystick but keep it within the boundaries of the base
        this.shaft.move(
            this.shaft.clamp(e.clientX, e.clientY, this.boundary),
            () => {
                if(typeof this.ondrag === 'function'){
                    this.ondrag();
                }
            }
        );

        return this;
    }

    /**
     * If this element is clicked don't have pause activatable
     *
     * @param target
     * @returns {boolean}
     */
    unpausable(target){
        return target !== this.base.element && !this.base.element.contains(target);
    }

    /**
     * Remove all components of the joystick
     *
     * @returns {Joystick}
     */
    remove(){
        this.detachEvents();

        this.base.removeElement();
        this.base = null;

        this.shaft.removeElement();
        this.shaft = null;

        return this;
    }
}
