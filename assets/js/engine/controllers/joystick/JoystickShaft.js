import JoystickElement from "./JoystickElement";

export default class JoystickShaft extends JoystickElement {
    /**
     * Restrict the joystick shaft's movement to a given boundary
     *
     * @param x
     * @param y
     * @param boundary
     * @returns {{x: number, y: number}}
     */
    clamp(x, y, boundary){
        // Trigonometry time!
        // - Who says what you learn in school won't become useful :D
        let diff = {
            x: x - this.rect.center.x,
            y: y - this.rect.center.y,
        };

        // Get the distance between the cursor and the center
        let distance = Math.sqrt(
            Math.pow(diff.x, 2) + Math.pow(diff.y, 2)
        );

        // Get the angle of the line
        let angle = Math.atan2(diff.x, diff.y);
        // Convert into degrees!
        this.current.angle = 180 - (angle * 180 / Math.PI);

        // If the cursor is distance from the center is
        // less than the boundary, then return the diff
        //
        // Note: Boundary = radius
        if(distance < boundary){
            this.current.percentage = (distance / boundary) * 100;
            return this.current.vector = diff;
        }

        // If it's a longer distance, clamp it!
        this.current.percentage = 100;

        return this.current.vector = {
            x: Math.sin(angle) * boundary,
            y: Math.cos(angle) * boundary
        };
    }

    /**
     * Animate the position of the joystick shaft to a point
     *
     * @param to
     * @param callback
     * @returns {*}
     */
    move(to, callback){
        this.element.style.transform = `translate3d(${to.x}px, ${to.y}px, 0px)`;

        if(typeof callback === 'function'){
            callback();
        }

        return this;
    }
}
