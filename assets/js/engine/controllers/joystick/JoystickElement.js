import GameElement from '../../core/GameElement';

export default class JoystickElement extends GameElement {
    constructor(selector, parent){
        super(selector, parent);
        this.current = this.original;
    }

    /**
     * Make an original array of the values that will be changed on moving the
     * joystick to compare against and to reset
     *
     * @returns {{vector: {x: number, y: number}, angle: number, percentage: number}}
     */
    get original(){
        return {
            vector: {
                x: 0,
                y: 0
            },
            angle: 0,
            percentage: 0
        };
    }

    /**
     * Extend the bounding client rect with a center point and radius
     *
     * @returns {*}
     */
    calculateRect(){
        let rect = this.element.getBoundingClientRect();

        return this.rect = Object.assign(
            rect,
            {
                center: {
                    x: rect.left + rect.width / 2,
                    y: rect.top + rect.height / 2
                },
                radius: rect.width / 2 // Improve this
            }
        );
    }

    /**
     * Remove the touch action capabilities for PEP compliance
     *
     * @returns {Element|*}
     */
    removeTouch(){
        this.element.setAttribute('touch-action', 'none');

        return this.element;
    }
}
