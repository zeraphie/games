import Helper from "../../../dependencies/Helper";

export default class Keyboard {
    constructor(){
        this.type = 'keyboard';

        this.onkeyup = () => {};
        this.keyup = this.keyup.bind(this);
    }

    keyup(e){
        if(typeof this.onkeyup === 'function'){
            this.onkeyup(
                Helper.ObjectKeyByValue(this.constructor.mapping, e.which)
            );
        }
    }

    /**
     * Attach the events to record keypresses
     *
     * @returns {Keyboard}
     */
    attachEvents(){
        document.addEventListener('keyup', this.keyup, false);

        return this;
    }

    /**
     * Detach the keyup event
     *
     * @returns {Keyboard}
     */
    detachEvents(){
        document.removeEventListener('keyup', this.keyup);

        return this;
    }

    remove(){
        this.detachEvents();

        return this;
    }

    unpausable(){
        return true;
    }

    static get mapping(){
        return {
            UP: 38,
            RIGHT: 39,
            DOWN: 40,
            LEFT: 37,
            W: 87,
            D: 68,
            S: 83,
            A: 65,
            SHIFT: 16,
            JUMP: 32
        };
    }
}
