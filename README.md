# Games
This is a modular Game Engine that utilizes ES6 functionality to provide an easy to use engine

*Note: There are readmes in a majority of places that have a short description of what the files of a folder are meant to be doing*

## Installation
Install with `npm` and if you want to start building your own game, use `gulp`
```bash
npm install
gulp
```

## Notes
1. This uses browserify to make use of the import/export syntax of es6
2. Import files with the `./` prefix in order to make sure it matches from the current directory
